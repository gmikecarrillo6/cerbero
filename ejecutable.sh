current_time=$(date "+%Y.%m.%d-%H.%M.%S")
mvn clean verify serenity:aggregate -Dserenity.outputDirectory="src/test/resources/reports/report_$current_time"