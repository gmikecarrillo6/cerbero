package task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import ui.HomePage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Access implements Task {
    private final String appButton;
    HomePage homePage;
    public Access(String appButton){
        this.appButton=appButton;
    }

    @Step("{0} la opcion '#opcion'")
    public <T extends Actor> void performAs(T actor) {
       actor.attemptsTo(Open.browserOn().the(homePage));
        actor.attemptsTo(Click.on(HomePage.APP_SELECTOR));
        switch (appButton){
            case "Recepcion":
                actor.attemptsTo(Click.on(HomePage.RECEPTION_APP_ICON));
                break;
            case "Pacientes":
                actor.attemptsTo(Click.on(HomePage.PATIENT_APP_ICON));
                break;
            case "RRHH":
                actor.attemptsTo(Click.on(HomePage.RRHH_APP_ICON));
                break;
        }
    }

    public static Access withAppOption(String option){
        return instrumented(Access.class, option);
    }
}
