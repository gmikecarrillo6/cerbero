package question;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.questions.targets.TheTarget;
import ui.HomePage;


import java.util.List;

public class TheHomePageDetails {
    public static Question<String> welcomeMessage() {
        return TheTarget.textOf(HomePage.WELCOME_LABEL);
    }
}