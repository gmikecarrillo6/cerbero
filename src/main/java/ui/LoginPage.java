package ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://his-stg.redsanitaria.com/login")
public class LoginPage extends PageObject{
    public static final Target EMAIL_TEXTFIELD = Target
            .the("user textfield")
            .located(By.cssSelector("[name=\"email\"]"));

    public static final Target PASSWORD_TEXTFIELD = Target
            .the("password textfield")
            .located(By.cssSelector("[name=\"password\"]"));

    public static final Target SUBMIT_BUTTON = Target
            .the("login button")
            .located(By.cssSelector("[type=\"submit\"]"));
}
