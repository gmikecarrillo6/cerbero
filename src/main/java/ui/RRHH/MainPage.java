package ui.RRHH;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MainPage {
    public static final Target RRHH_MAIN_PAGE_LABEL = Target.the("RRHH main page title")
            .located(By.cssSelector("h6.hidden"));
}
