package ui.Administracion;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MainPage {

    public static final Target RECEPTION_MAIN_PAGE_LABEL = Target.the("Reception main page title")
            .located(By.cssSelector("h6.hidden"));
}
