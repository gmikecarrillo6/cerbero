package ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://his-stg.redsanitaria.com/home")
public class HomePage extends PageObject {
    public static final Target WELCOME_LABEL = Target.the("friend request icon")
            .located(By.cssSelector("h4.py-0"));
    public static final Target APP_SELECTOR = Target.the("app selector menu")
            .located(By.cssSelector("[class*=\"MuiButton-contained-\"]"));
    public static Target RECEPTION_APP_ICON = Target.the(("patient app icon"))
            .located(By.cssSelector("ul[class*=\"MuiList-root-\"] button:nth-child(1)"));
    public static Target PATIENT_APP_ICON = Target.the(("patient app icon"))
            .located(By.cssSelector("ul[class*=\"MuiList-root-\"] button:nth-child(2)"));
    public static Target RRHH_APP_ICON = Target.the(("patient app icon"))
            .located(By.cssSelector("ul[class*=\"MuiList-root-\"] button:nth-child(3)[style]"));
}
