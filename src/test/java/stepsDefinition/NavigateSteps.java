package stepsDefinition;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.questions.page.TheWebPage;
import net.serenitybdd.screenplay.questions.targets.TheTarget;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.Managed;

//import org.junit.Before;
import org.openqa.selenium.WebDriver;
import question.TheHomePageDetails;
import question.TheMainPageDetails;
import task.Access;
import task.LogIn;
import task.Navigate;
import ui.HomePage;
import ui.RRHH.MainPage;

import static net.serenitybdd.screenplay.EventualConsequence.eventually;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.containsText;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.StringContains.containsString;

public class NavigateSteps {
    @Managed
    WebDriver browser;

    Actor sam = Actor.named("Sam");

    @Given("^I navigate to the Login page$")
    public void I_navigate_to_the_Login_page() {
        sam.can(BrowseTheWeb.with(browser));
        sam.wasAbleTo(Navigate.loginPage());
    }

    @When("^I log in with '(.*)' and '(.*)'$")
    public void I_log_in_with_email_and_password(String email, String password) throws Throwable {
        sam.attemptsTo(LogIn.withCredentials(email, password));
    }

    @Then("^Home page should be displayed$")
    public void Home_page_should_be_displayed() throws Throwable {
        //sam.should(eventually(seeThat(TheWebPage.title(), containsString("(2) Facebook"))));
        //sam.should(seeThat(the(HomePage.WELCOME_LABEL), containsText("MORENO , HERNAN")));
        //sam.should(seeThat(TheTarget.valueOf(HomePage.WELCOME_LABEL), equalTo(   "MORENO , HERNAN")));
        sam.should(eventually(seeThat(TheHomePageDetails.welcomeMessage(), containsString(   "Bienvenido, Ivan"))));
    }

    @When("^Selecciono la opcion '(.*)'$")
    public void Selecciono_opcion_de_navegacion(String option){
        sam.attemptsTo((Access.withAppOption(option)));
    }

    @Then("puedo observar la pagina principal de '(.*)'")
    public void Valido_acceso_a_la_pantalla(String page){
        sam.should(eventually(seeThat(TheMainPageDetails.title(page),containsString(page))));
    }
}