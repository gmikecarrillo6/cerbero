
Feature: Login functionality
  As a customer
  In order to use the application
  I want to login with email and password
  {Examples} example mike
Background:
  Given I navigate to the Login page
  When I log in with 'icaneri@redsanitaria.com' and 'Ssilvin6'
  Then Home page should be displayed

  @Smoke @UI
  Scenario: Quiero poder navegar por libremente por las paginas de Red sanitaria
    When Selecciono la opcion 'Pacientes'
    Then puedo observar la pagina principal de 'Pacientes'
    When Selecciono la opcion 'RRHH'
    Then puedo observar la pagina principal de 'Personal'
    When Selecciono la opcion 'Recepcion'
    Then puedo observar la pagina principal de 'Recepción'
