Feature: Obtener listado de Centros medicos

    @API
    Scenario: Intento obtener un listado de los diferentes centros medicos sin credenciales
      When intento acceder al listado de centros medicos sin credenciales
      Then valido el error al querer acceder al listado de centros medicos sin credenciales

    @API
    Scenario: Quiero poder obterner un listado de los diferentes centros medicos que existen
      Given Ingreso con usuario 'icaneri@redsanitaria.com' y clave 'Ssilvin6'
      When cuando consulto el listado de centros-medicos
      Then valido los siguentes datos del centro medico:
      |idCentro|   denomCentro|
      |     118|CAPS Dr.Caeiro|